﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Calculator;
using NUnit.Framework;

namespace CalculatorTests
{
   
    [TestFixture]

    public class CalculatorTests
    {
        MyCalculator calculator;

        [OneTimeSetUp] 

        public void RunBeforeAllTests()
        {
            calculator = new MyCalculator();
        }

        [Test, Order(1)]
        public void SumTest()
        {
            Assert.That(7.0, Is.EqualTo(calculator.Sum(2.0, 5.0)));
        }

        [Test, Order(3)]
        public void MultiplyTest()
        {
            Assert.AreSame(2.26, calculator.Multiplication(1.5, 1.5), "Actual and Expected are not same");
        }

        [Test, Order(2)]
        public void SubtractTest()
        {
            Assert.AreEqual(1.0, calculator.Subtraction(2.0, 1.0));
        }

        [Test, Order(4)]
        public void DivideTest()
        {
            Assert.AreEqual(1.0, calculator.Division(1, 0));
        }

    }
}

