﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator;
using NUnit.Framework;

namespace CalculatorTests
{
    [SetUpFixture]
    public class MySetUpClass
    {

        [OneTimeSetUp]
        public void RunBeforeAllTests()
        {
            // ...
        }

        [OneTimeTearDown]
        public void RunAfterAllTests()
        {
            // ...
        }
    }
}
