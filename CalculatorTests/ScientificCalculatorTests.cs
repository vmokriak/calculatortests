﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator;
using NUnit.Framework;

namespace ScientificCalculatorTests
{
    [TestFixture]

    public class ScientificCalculatorTests
    {
        ScientificCalculator calculator;
        int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        [OneTimeSetUp]

        public void RunBeforeAllTests()
        {
            calculator = new ScientificCalculator();
        }

        [Test, Order(1)]
        public void PowTest()
        {
            Assert.AreSame(1024.0, calculator.Pow(2.0, 10));
        }

        [Test, Order(2)]
        public void SqrtTest()
        {
            Assert.AreEqual(25.1, calculator.Sqrt(5.0));
        }

        [Test, Order(3)]
        public void PercentTest()
        {
            Assert.AreEqual(20, calculator.Percent(2.1, 10));
        }

        [Test, Order(4)]
        public void ArrSumTest()
        {
            Assert.That(55, Is.EqualTo(calculator.ArraySum(array)));
        }

        [Test, Order(5)]
        public void ArrMaxTest()
        {
            Assert.AreEqual(0, calculator.ArrayMax(array));
        }

        [Test, Order(6)]
        public void ArrMinTest()
        {
            Assert.AreEqual(1, calculator.ArrayMin(array));
        }

        [Test, Order(7)]
        public void IsRangeTest()
        {
            Assert.That(array, Is.All.InRange(0, 100));
        }

        [Test, Order(8)]
        public void GreaterOrLessTest()
        {
            Assert.That(5, Is.GreaterThan(calculator.ArrayMin(array)).And.LessThan(calculator.ArrayMax(array)));
        }
    }
}
