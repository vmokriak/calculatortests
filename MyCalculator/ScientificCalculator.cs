﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class ScientificCalculator : MyCalculator
    {
        public double Pow(double x, int y)
        {
            return Math.Pow(x, y);
        }

        public double Sqrt(double x)
        {
            return Math.Sqrt(x);
        }

        public double Percent(double number, double total)
        {
            return (number / total) *100;
        }

        public int ArraySum(int[] array)
        {
            return array.Sum();
        }

        public int ArrayMax(int[] array)
        {
            return array.Max();
        }

        public int ArrayMin(int[] array)
        {
            return array.Min();
        }
    }
}
